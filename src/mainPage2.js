import React from 'react';
import logo from './logoiD3.0.png';
import coeur from './coeur.png';
import comm from './com2.png';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';


class PageAccueil extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.navigationPannel}
        {this.props.pagePrincipale}
      </div>
    );
  }
}

export default PageAccueil;
