import React from 'react';
import logo from './logo.svg';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';


class PageAccueil extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div class="row">
        {this.props.navigationPannel}
        <div class="col-6 border-notop border-clair white-text p-0">
          {this.props.pagePrincipale}
        </div>
        <div class="col-3white-text">
          {this.props.pageDroite}
        </div>
      </div>
    );
  }
}

export default PageAccueil;
