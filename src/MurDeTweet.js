import React from 'react';
import logo from './logoiD3.0.png';
import coeur from './coeur.png';
import comm from './com2.png';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

// import './mainPage.css';
import 'bootstrap/dist/css/bootstrap.css';

class MurDeTweet extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div>
        <div class="row p-b-100">
          <div class="col-6">
            <h1 class="display-1 p-l-100 p-t-100">Hi ! What's up today ?</h1>
            <h1 class="font-swag p-l-100" style={{"padding":"50px"}}>Share your thoughts with your friends</h1>
          </div>
          <div class="col-6">
            <img class="p-l-100" src={logo} width="auto" height="750" alt=""/>
          </div>
        </div>
        <ul class="non">
          <li>
            <div class="row messageBox">
              <div class="col-6 message">
                <div class="display-4 font-weight-bolder h-15" style={{"margin-bottom":"20px"}}>Adam_the_goat</div>
                <div class="font-swag overflow-auto h-70" style={{"margin-bottom":"50px"}}>{this.props.message}</div>
                <div class="d-flex justify-content-around h-15" style={{"padding-bottom":"15px"}}>
                  <img src={coeur} width="auto" height="40" alt=""/>
                  <img src={comm} width="auto" height="40" alt=""/>
                </div>
              </div>
              <div class="col-6 com overflow-auto">
                <ul class="non">
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <li>
            <div class="row messageBox">
              <div class="col-6 message">
                <div class="display-4 font-weight-bolder h-15" style={{"margin-bottom":"20px"}}>Adam_the_goat</div>
                <div class="font-swag overflow-auto h-70" style={{"margin-bottom":"50px"}}>I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !</div>
                <div class="d-flex justify-content-around h-15" style={{"padding-bottom":"15px"}}>
                  <img src={coeur} width="auto" height="40" alt=""/>
                  <img src={comm} width="auto" height="40" alt=""/>
                </div>
              </div>
              <div class="col-6 com overflow-auto">
                <ul class="non">
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <li>
            <div class="row messageBox">
              <div class="col-6 message">
                <div class="display-4 font-weight-bolder h-15" style={{"margin-bottom":"20px"}}>Adam_the_goat</div>
                <div class="font-swag overflow-auto h-70" style={{"margin-bottom":"50px"}}>I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !I just ate an apple, it was fun !</div>
                <div class="d-flex justify-content-around h-15" style={{"padding-bottom":"15px"}}>
                  <img src={coeur} width="auto" height="40" alt=""/>
                  <img src={comm} width="auto" height="40" alt=""/>
                </div>
              </div>
              <div class="col-6 com overflow-auto">
                <ul class="non">
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                    <div>nice !</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">Georgia West</div>
                    <div>love it ! #TeamApple</div>
                  </li>
                  <li style={{"padding-bottom":"15px"}}>
                    <div class="font-weight-bolder">LebronJames4783</div>
                    <div>Not you adam... i thought you were in the banana team..</div>
                  </li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

export default MurDeTweet;
