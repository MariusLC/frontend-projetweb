import React from 'react';
import logo from './logo.svg';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

// import './mainPage.css';
import 'bootstrap/dist/css/bootstrap.css';

class NavigationPannel extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div class="col-3 p-0">
        <h1 class="h3 mb-0 font-weight-normal text-center m-4 white-text">NavigationPannel</h1>
        <ul class="nav flex-column">
          <li class="nav-item"><HomeButton
          home = {this.props.home}
          class="btn btn-lg btn-thoughts btn-petitgros btn-centered font-weight-bold mtb-10">
          </HomeButton></li>
          <li class="nav-item"><ProfileButton
          profile = {this.props.profile}
          class="btn btn-lg btn-thoughts btn-petitgros btn-centered font-weight-bold mtb-10">
          </ProfileButton></li>
          <li class="nav-item"><PostMessageButton
          postMessage = {this.props.postMessage}
          class="btn btn-lg btn-thoughts btn-petitgros btn-centered font-weight-bold mtb-10">
          </PostMessageButton></li>
          <li class="nav-item"><LogoutButton
          setLogout = {this.props.setLogout}
          class="btn btn-lg btn-red btn-petitgros btn-centered font-weight-bold mtb-10">
          </LogoutButton></li>
        </ul>
      </div>
    );
  }
}

export default NavigationPannel;
