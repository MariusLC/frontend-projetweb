import React from 'react';
import logo from './logo.svg';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

import 'bootstrap/dist/css/bootstrap.css';

class PostMessagejs extends React.Component {
  constructor(props){
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    this.props.post(this.state.value);
  }

  render() {
    return (
      <form class="p-0">
        <h1 class="h3 font-weight-normal text-center m-0 p-4 white-text border-b border-clair">Post Message</h1>
        <div class="tweet border-clair border-b p-2">
          <div class="h5 font-weight-bold white-text">{this.props.login}</div>
          <textarea class="font-weight-normal white-text p-2 postMessage" maxlength={140} placeholder="What are you thinking about ?" value={this.state.value} onChange={this.handleChange} required autofocus/>
          <button type="submit" class="btn btn-lg btn-red btn-petitgros btn-right font-weight-bold" style={{"margin-top":"10px"}} onClick={this.handleSubmit}>Post</button>
        </div>
      </form>
    );
  }
}

export default PostMessagejs;
