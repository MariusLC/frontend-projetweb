import React from 'react';
import logo from './logo.svg';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

import 'bootstrap/dist/css/bootstrap.css';

class Loginjs extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <form class="form-signin">
        <h1 class="h3 mb-3 font-weight-normal text-center white-text">Please sign in</h1>
        <label class="sr-only">Login</label>
        <input type='text' class="form-control border-bottom border-dark mb-2 rounded" placeholder="Login" required autofocus/>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control border-bottom border-dark mb-0 rounded" placeholder="Password" required/>
        <button type="button" class="btn btn-link text-left mt-0 ml-0 mb-3" style={{color:'#17a2b8'}}>Forgotten Password ?</button>
        <button type="submit" class="btn btn-lg btn-info btn-block" onClick={this.props.getConnected}>Signin</button>
        <p class="text-center text-info mb-2 mt-2">or</p>
        <button type="button" class="btn btn-lg btn-outline-info btn-block" onClick={this.props.register}>Register</button>
      </form>
    );
  }
}

export default Loginjs;
