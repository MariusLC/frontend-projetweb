import React from 'react';
import logo from './logo.svg';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

import 'bootstrap/dist/css/bootstrap.css';

class PostMessagejs extends React.Component {
  constructor(props){
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    this.props.post(this.state.value);
  }

  render() {
    return (
      <form >
        <div class="postBox">
          <div class="display-4 font-weight-bolder h-15" style={{"margin-bottom":"20px"}}>{this.props.login}</div>
          <textarea class="font-swag overflow-auto text-post" maxlength={140}
          placeholder="What's on your mind Today ?" value={this.state.value} onChange={this.handleChange} required autofocus
          />
          <div class="h-15">
            <button type="submit" class="btn btn-post font-swag font-weight-bolder" onClick={this.handleSubmit}>Post</button>
          </div>
        </div>
      </form>


    );
  }
}

export default PostMessagejs;
