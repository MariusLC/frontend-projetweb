import React from 'react';
import logo from './logo.svg';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

import './signin.css'

class Registerjs extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <form class="form-signin">
        <h1 class="h3 mb-0 font-weight-normal text-center white-text">Please Register</h1>
        <div class="form-row">
          <div class="form-group col-6 mb-2">
            <label>Name</label>
            <input class="form-control" placeholder="Name"/>
          </div>
          <div class="form-group col-6 mb-2">
            <label>First name</label>
            <input class="form-control" placeholder="First name"/>
          </div>
        </div>
        <label class="sr-only">Login</label>
        <input class="form-control border-bottom border-dark mb-2 rounded" placeholder="Login" required autofocus/>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control border-bottom border-dark mb-2 rounded" placeholder="Password" required/>
        <button type="submit" class="btn btn-lg btn-info btn-block mt-4" onClick={this.props.getRegistered}>Register</button>
        <p class="text-center text-info mb-2 mt-2">or</p>
        <button type="button" class="btn btn-lg btn-outline-info btn-block" onClick={this.props.login}>Sign in</button>
      </form>
    );
  }
}

export default Registerjs;
