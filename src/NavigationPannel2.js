import React from 'react';
import logo from './logoiD3.0.png';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

import './projetv2.css';
import 'bootstrap/dist/css/bootstrap.css';

class NavigationPannel extends React.Component {
  constructor(props){
    super(props);
  }

  funAlertClic(){
    alert('clic');
  }

  render() {
    return (
      <nav class="navbar navbar-expand-lg navbar-light fixed-top navBar lead">
        <a class="navbar-brand" href="#">
          <img src={logo} width="100%" height="70" alt=""/>
        </a>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-item nav-link active" href="#" onClick={this.props.home} style={{"padding-left":"40px"}}>Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-item nav-link" href="#" onClick={this.props.profile} style={{"padding-left":"40px"}}>Profile</a>
            </li>
            <li class="nav-item ">
                <a class="nav-item nav-link" href="#" onClick={this.props.postMessage} style={{"padding-left":"40px"}}>Post Message</a>
            </li>
            <li class="nav-item ">
                <a class="nav-item nav-link" href="#" onClick={this.props.setLogout} style={{"padding-left":"40px"}}>Logout</a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavigationPannel;
