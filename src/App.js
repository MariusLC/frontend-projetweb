import React from 'react';
import logo from './logo.svg';

import PageAccueil from './mainPage';
import NavigationPannel from './NavigationPannel';
import Registerjs from './register';
import Loginjs from './login';
import PostMessagejs from './postMessage';
import Profilejs from './Profile';

import './mainPage.css';
import './signin.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

class MainPage extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isConnected : false,
      pageCourante : 'connexion',
      nom : 'Dupont',
      prenom : 'Pierre-Alexandre',
      login : 'xX_DarKnight_Xx',
      message : 'yo tout le monde !'
    }
    // this.getConnected = this.getConnected.bind(this);
  }

  // Trois actions de connexion/deconnexion/inscriptions
  getConnected(){
    this.setState({
      isConnected : true,
      pageCourante : 'home',
    });
  }

  setLogout(){
    this.setState({
      isConnected : false,
      pageCourante : 'connexion',
    });
  }

  getRegistered(){
    this.setState({
      isConnected : false,
      pageCourante : 'connexion',
    });
  }

  setName(name){
    this.setState({
      nom : name,
    });
  }

  setFirstName(firstname){
    this.setState({
      prenom : firstname,
    });
  }

  setLogin(login){
    this.setState({
      login : login,
    });
  }

  post(message){
    this.setState({
      message : message,
      pageCourante : 'home',
    });
  }

  // Toutes les actions de navigations qui changent la pageCourante
  login(){
    this.setState({
      pageCourante : 'connexion',
    });
  }

  register(){
    this.setState({
      pageCourante : 'inscription',
    });
  }

  profile(){
    this.setState({
      pageCourante : 'profile',
    });
  }

  setProfile(){
    this.setState({
      pageCourante : 'setProfile',
    });
  }

  postMessage(){
    this.setState({
      pageCourante : 'postMessage',
    });
  }

  home(){
    this.setState({
      pageCourante : 'home',
    });
  }

  render() {
    if (this.state.isConnected){
      let pagePrincipale;
      if (this.state.pageCourante == 'home'){
        pagePrincipale=
          <Home
          message = {this.state.message}>
          </Home>
      } if (this.state.pageCourante == 'profile'){
        pagePrincipale=
          <Profile
            setProfile = {() => this.setProfile()}
            nom = {this.state.nom}
            prenom = {this.state.prenom}
            login = {this.state.login}>
          </Profile>
      } if (this.state.pageCourante == 'postMessage'){
        pagePrincipale=
          <PostMessage
          post = {(message) => this.post(message)}
          login = {this.state.login}>
          </PostMessage>
      } if (this.state.pageCourante == 'setProfile'){
        pagePrincipale=
          <SetProfile
            nom = {this.state.nom}
            prenom = {this.state.prenom}
            login = {this.state.login}
            setName = {() => this.setName()}
            setFirstName = {() => this.setFirstName()}
            setLogin = {() => this.setNsetLoginame()}>
          </SetProfile>
      }
      let pageDroite = <h1>Je suis la page de droite</h1>;
      return (
        <PageAccueil
          navigationPannel =
            <NavigationPannel
            getConnected = {() => this.getConnected()}
            getRegistered = {() => this.getRegistered()}
            setLogout = {() => this.setLogout()}
            login = {() => this.login()}
            register = {() => this.register()}
            profile = {() => this.profile()}
            home = {() => this.home()}
            postMessage = {() => this.postMessage()}
            isConnected = {this.state.isConnected}
            pageCourante = {this.state.pageCourante}
            ></NavigationPannel>
          pagePrincipale = {pagePrincipale}
          pageDroite = {pageDroite}
        ></PageAccueil>
      );
    } else {
      if (this.state.pageCourante == 'inscription'){
        return (
          <Register
            getRegistered = {() => this.getRegistered()}
            login = {() => this.login()}>
          </Register>);
      } else if (this.state.pageCourante == 'connexion'){
        return (
          <Login
            getConnected = {() => this.getConnected()}
            register = {() => this.register()}>
          </Login>);
      }
    }
  }
}

class LogoutButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick= {this.props.setLogout} class= {this.props.class}>
        {"Logout"}
      </button>
    );
  }
}

class LoginButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick = {this.props.login} class= {this.props.class}>
        {"Se Connecter"}
      </button>
    );
  }
}

class Login extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <Loginjs
      getConnected = {this.props.getConnected}
      register = {this.props.register}
      ></Loginjs>
    );
  }
}

class RegisterButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick= {this.props.register} class= {this.props.class}>
        {"S'inscrire"}
      </button>
    );
  }
}

class Register extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <Registerjs
      getRegistered = {this.props.getRegistered}
      login = {this.props.login}
      ></Registerjs>
    );
  }
}

class ProfileButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick= {this.props.profile} class= {this.props.class}>
        {"Profile"}
      </button>
    );
  }
}

class Profile extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <ul>
        <li>Nom : {this.props.nom}</li>
        <li>Prenom : {this.props.prenom}</li>
        <li>Login : {this.props.login}</li>
        <SetProfileButton
        setProfile = {this.props.setProfile}>
        </SetProfileButton>
      </ul>
    );
  }
}

class SetProfileButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick= {this.props.setProfile} class= {this.props.class}>
        {"setProfile"}
      </button>
    );
  }
}

class SetProfile extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      // BOUTONS A IMPLEMENTER
      <div>
        <p>{this.props.nom}</p>
        <form method='post' action='setName'>
        Nom : <input type='text' name='nom' placeholder={this.props.nom}/><br  />
        <input type='submit' value='set name'/><br />
        </form>
        <p>{this.props.prenom}</p>
        <form method='post' action='setFirstName'>
        Prenom : <input type='text' name='nom' placeholder={this.props.prenom}/><br  />
        <input type='submit' value='set first name'/><br />
        </form>
        <p>{this.props.login}</p>
        <form method='post' action='setLogin'>
        Login : <input type='text' name='nom' placeholder={this.props.login}/><br  />
        <input type='submit' value='set login'/><br />
        </form>
        <button onClick= {this.props.profile}>
          {"Save Changes"}
        </button>
      </div>
    );
  }
}

class HomeButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick= {this.props.home} class= {this.props.class}>
        {"Home"}
      </button>
    );
  }
}

class Home extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    let text = "Pour certains types déléments <input>, les valeurs saisies autorisées dépendent de la locale utilisée. Ainsi, dans certaines locales, 1,000.00 est un nombre valide alors que dans dautres, il faudra avoir saisi 1.000,00 pour exprimer le même nombre.Firefox utilise la méthode heuristique suivante afin de déterminer la locale pour laquelle valider la saisie de l'utilisateur (au moins dans le cas de type='number')";
    return (
      // <PageAccueil />
      <ul class="p-0 non">
        <h1 class="h3 font-weight-normal text-center m-0 p-4 white-text border-b border-clair">Mur de Tweets</h1>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{this.props.message}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
        <li class="tweet border-clair border-b p-2"><div class="h5 font-weight-bold white-text">Username, login</div>
          <div class="font-weight-normal white-text p-2">{text}</div>
        </li>
      </ul>
    );
  }
}

class PostMessageButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick= {this.props.postMessage} class= {this.props.class}>
        {"postMessage"}
      </button>
    );
  }
}

class PostMessage extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <PostMessagejs
      post = {this.props.post}
      login = {this.props.login}
      ></PostMessagejs>
    );
  }
}

export {HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage};
export default App;
