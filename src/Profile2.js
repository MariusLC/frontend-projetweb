import React from 'react';
import logo from './logoiD3.0.png';
import coeur from './coeur.png';
import comm from './com2.png';
import {LoginButton, SigninButton, HomeButton, ProfileButton, PostMessageButton, LogoutButton, MainPage} from './App';

import 'bootstrap/dist/css/bootstrap.css';

class Profilejs extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      list : "coeur",
    }
  }

  coeurf () {
    alert('clic');
    this.setState({
      list : "coeur",
    });
  }

  commf () {
    alert('clic');
    this.setState({
      list : "comm",
    });
  }
  // <a class="navbar-brand" href="#"><img alt="Brand" src={coeur} height="40" width="auto" onClick={this.funalert("coeur")}/></a>
  // <a class="navbar-brand" href="#"><img alt="Brand" src={comm} height="40" width="auto" onClick={this.funalert("comm")}/></a>

  render() {
    if (this.state == "coeur"){
      let coeur_color = "#000";
      let comm_color = "#fff";
    } else {
      let coeur_color = "#fff";
      let comm_color = "#000";
    }
    return (
      <form >
        <div class="row profileBox">
          <div class="col-6" style={{"padding-top":"80px","padding-bottom":"0px"}}>
            <div class="message h-70">
              <div class="font-swag font-weight-light">nom :</div>
              <div class="font-weight-bold" style={{"margin-bottom":"20px"}}>{this.props.nom}</div>
              <div class="font-swag font-weight-light">prenom :</div>
              <div class="font-weight-bold" style={{"margin-bottom":"20px"}}>{this.props.prenom}</div>
              <div class="font-swag font-weight-light">login :</div>
              <div class="font-weight-bold">{this.props.login}</div>
            </div>
            <button type="button" class="btn btn-post font-swag font-weight-bolder" style={{"margin-top":"10px"}} onClick={this.setProfile}>Set your Profile</button>
          </div>
          <div class="col-6 com" style={{"margin-top":"20px"}}>
            <div class="navbar d-flex justify-content-around" style={{"padding-bottom":"15px"},{"height":"10%"}}>
               <a class="navbar-brand" style={{"background-color":"#000"}}href="#"><img alt="Brand" src={coeur} height="40" width="auto"/></a>
              {comm}
            </div>
            <div class="overflow-auto h-70">
              <ul class="non">
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">Georgia West</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">LebronJames4783</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">Georgia West</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">LebronJames4783</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">xXDarkKnight123Xx</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">Georgia West</div>
                </li>
                <li style={{"padding-bottom":"15px"}}>
                  <div class="font-weight-bolder">LebronJames4783</div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </form>

    );
  }
}

export default Profilejs;
